package kasirpos;
//Import Library SQL (First)
//Jika ingin diimport semua (Syntax : java.sql.*)
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import static net.proteanit.sql.DbUtils.resultSetToTableModel;
/**
 *
 * @author code-developer
 */
public class ManipulationDB {
    //Buat Variable Untuk Keperluan Manipulasi Database
    static Connection CONN;
    static ResultSet RS;
    static Statement STM;
    static PreparedStatement PST;
    
    public static void Koneksi(){
        String URL = "jdbc:mysql://localhost/KasirPOS?serverTimezone=UTC"; //Initialize URL || Perlu menggunakan param ?serverTimezone=UTC karena serverTimezone terdeteksi WIB (Tidak Support) harus dipass ke UTC. (If Any Request)
        String USER = "developer";  //Initialize User
        String PASSWORD = "developer"; //Initialize Password
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");  //Mendaftarkan Driver SQL (If Any Request)
            CONN = DriverManager.getConnection(URL,USER,PASSWORD); //Get Koneksi dengan parameter URL USER PASSWORD yang sudah dibuat
            STM = CONN.createStatement(); //Buat statement jalankan koneksi
        }
        catch(Exception ex){ //Jika ada kesalahan (Exception (Error Global) tampilkan error nya || Jika ingin spesifik gunakan SQLException (Dibaca Sendiri Di Internet :) ))
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    
    public static int IsAuthenticationUserLogin(String _u, String _p){
        try{
            String _query = "SELECT * FROM Users WHERE id='" + _u + "' AND password = '" + _p + "'";
            RS = STM.executeQuery(_query);
            if(RS.next()){
                return RS.getInt("jabatan");
            }
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
        return 0;
    }
    
    public static void CheckDataIfExist(){
        try{
            
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    
    public static void ShowDataTable(String _t, JTable _el){
        try{
            RS = STM.executeQuery("SELECT * FROM " + _t);
            _el.setModel(resultSetToTableModel(RS));
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    
    public static void InsertData(String _t, String _d){
        try{
            PST = CONN.prepareStatement("INSERT INTO "+ _t + " VALUES (" + _d + ")");
            PST.execute();
            JOptionPane.showMessageDialog(null, "Berhasil Menambah Data !");
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    
    public static void UpdateData(String _t, String _d, String _w){
        try{
            PST = CONN.prepareStatement("UPDATE "+ _t + " SET " + _d + " WHERE " + _w + "");
            PST.execute();
            JOptionPane.showMessageDialog(null, "Berhasil Memperbarui Data !");
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    
    public static void DeleteData(String _t, String _d){
        try{
            PST = CONN.prepareStatement("DELETE FROM "+ _t + " WHERE " + _d + "");
            PST.execute();
            JOptionPane.showMessageDialog(null, "Berhasil Menghapus Data !");
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    
    public static void AutoNumber(String _t, JTextField _j, String _k){
        try{
            RS = STM.executeQuery("SELECT MAX(id) FROM " + _t + "");   
            if(RS.next()){
                String kode = RS.getString(1).substring(3);
                String add = "" + (Integer.parseInt(kode) + 1);
                String res = "";
                    
                if(add.length() == 1){
                    res = "00";
                }
                else if(add.length() == 2){
                    res = "0";
                }
                else if(add.length() == 3){
                    res = "";
                }
                    _j.setText(_k+res+add);
            }
            else{
                _j.setText(_k+"001");
            }
        }
        catch(Exception ex){
            System.out.print("Terjadi Kesalahan : " + ex.toString());
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    }
}
